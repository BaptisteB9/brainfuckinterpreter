#!/bin/bash

function nextBracket () {
	local i=$1
	shift
	instructionArray=("$@")
	local count=0
	for a in ${instructionArray[@]}
	do
		if [ $a = ']' ] && [ $count -gt $i ]
		then
			return $count
		fi
		let count++
	done
	return 0
}

function previousBracket () {
	local i=$1
	shift
	instructionArray=("$@")
	local count=0
	local tmp=0
	for a in ${instructionArray[@]}
	do
		if [ $a = '[' ] && [ $count -lt $i ]
		then
			let tmp=$count
		fi
		let count++
	done
	return $tmp
}
# First test if we have one and only one parameter
if (($# == 0))
then
	echo "Error: Please enter one argument !"
elif (($# > 1))
then
	echo "Error: Please enter only one argument !"
fi
command_length=${#1}
command=$1

i=0
## Todo find some good regex
# until [ $i -eq $command_length ]; do
# 	if [[ ! ${command:$i:1} =~ [[]+-.\<\>] ]]
# 	then
# 		echo "Error1: The program contains unallowed characters: "${command:$i:1}
# 		break;
# 	fi
# 	let i++
# done
declare -a instructions=();
until [ $i -eq $command_length ]; do
	if [ ! ${command:$i:1} = '[' ] && [ ! ${command:$i:1} = ']' ] && [ ! ${command:$i:1} = '.' ] &&
		[ ! ${command:$i:1} = ',' ] && [ ! ${command:$i:1} = '-' ] && [ ! ${command:$i:1} = '>' ] &&
			[ ! ${command:$i:1} = '<' ] && [ ! ${command:$i:1} = '+' ]
	then
		echo "Error: The program contains unallowed characters: "${command:$i:1}
		exit;
	fi
	instructions+=(${command:$i:1})
	let i++
done
declare -a memory=(0)
instructionPointer=0
memoryPointer=0

until [ $instructionPointer -eq $command_length ]; do
	# echo instruction $instructionPointer ${instructions[$instructionPointer]} memory pointer $memoryPointer ${memory[$memoryPointer]}
	if [ ${instructions[$instructionPointer]} = '>' ]
	then
		let memoryPointer++
	fi
	if [ ${instructions[$instructionPointer]} = '<' ]
	then
		let memoryPointer--
	fi
	if [ ${instructions[$instructionPointer]} = '+' ]
	then
		let memory[$memoryPointer]++
	fi
	if [ ${instructions[$instructionPointer]} = '-' ]
	then
		let memory[$memoryPointer]--
	fi
	if [ ${instructions[$instructionPointer]} = '.' ]
	then
		temp=`echo -en \\\x && echo -e "obase=16; "${memory[$memoryPointer]} | bc`
		echo -en $temp
	fi
	if [ ${instructions[$instructionPointer]} = ',' ]
	then
		read
		let memory[$memoryPointer]=$REPLY
	fi
	if [ ${instructions[$instructionPointer]} = '[' ]
	then
		nextBracket $instructionPointer "${instructions[*]}"
		ret=$?
		if [ ${memory[$memoryPointer]} -eq 0 ]
		then
			let instructionPointer=$ret
		fi
	fi
	if [ ${instructions[$instructionPointer]} = ']' ]
	then
		previousBracket $instructionPointer "${instructions[*]}"
		ret=$?
		if [ ! ${memory[$memoryPointer]} -eq 0 ]
		then
			let instructionPointer=$ret
		fi
	fi
	let instructionPointer++
done